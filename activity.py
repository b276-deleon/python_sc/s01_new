name = "Earl John de Leon"
age = 28
occ = "Senior Quality Officer"
movie = "One More Chance"
rating = 99.9

print(f"I am {name} , and I am {age} years old, I work as a {occ} , and my rating for {movie} is {rating} %")

num1, num2, num3 = 7, 11, 9

print(num1*num2)
print(num1<num3)
print(num2+num3)